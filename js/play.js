//20180618 changelog
//In index
    // 增加 <script src="http://cdn.pubnub.com/pubnub.min.js"></script>
//In game.js
    //增加一些 game.global變數，且對某些初始化
//In waiting.js
    //新增 this.inf_ref ，此為存地圖資料的地方
    //update_current_data()
        //存chest_event、箱子的隨機位置、真鑰匙的編號
    //add new function
        //on_inf_ref_value_change:即時更新本地端資料
        //getChestPosition:因為我將play.js中隨機造箱子的部分移了過來，故直接將此函數移過來
    //on_ref_value_change():
        //在最後不關閉on_inf_ref_value_change，以使其能一直即時更新資訊
//在 load.js 的 create
    //新增 this.inf_ref ，此為存地圖資料的地方
    //更正無法開新房的問題(之前要開新房時總是會顯示"this game is playing.")
        //這部分只更改了 條件: total_ready == snapshot.numChildren() && total_ready
    //增加讀取各角色位置的code
    //若遊戲將直接開始，將會從firebase讀取chest_events、object_pos、true_key
//In play.js 
    //新增一些與物品位置有關的變數
    //新增 this.inf_ref ，此為存地圖資料的地方
    //在preload()和create()內增加pubnub和相關參數
    //update()
        //將nail改為用陣列存，並在此處理nail陣列元素的剃除
    //moveplayer()
        //大改移動的判斷方式，並在移動的同時送出移動訊息
        //此外也增加了is_move和last_move 以在停止不動時送出停下的訊息
    //setkeys()
        //將所有使用道具類的key所呼叫的函數改為using_item，往自己的頻道送出訊息
    //setplayers()
        //新增其他三個角色，並以陣列儲存
    //setitems()
        //將隨機生成箱子的部分移至waiting，在這裡只讀位置並建立箱子
        //建立已被放置在地圖上的物體
        //增加鑰匙的group
    //checkBox() 改為只送訊息
    //nail_end() 只改變生存狀態，以讓update判斷是否該捨棄該元素
    //death() 改為只傳送死亡訊息
    //gameOver() 真的會把你趕出遊戲XD 直接把你從firebase刪掉了XD
    //disappear(item)、appear(item) 改為判斷每個玩家
    //shockEffect() 直接掛掉
    //CheckKey() 改為和game.global.true_key判斷是否同
    //add new function
        //using_item: 用來送出使用了什麼item的訊息
        //chest_open: 播開寶箱動畫&傳送已開的訊息
        //find_machine:因為覺得CurrentOn、CurrentOff的判斷式覺得煩躁，就把它拿出來做function
        //listen_to_pubnub:多人
        //show_item:
            //這是將原本在checkBox()內的道具出現部分拿來做成一個function
        //keyEffect: 撿鑰匙
    //add new parameters and 增加或改變一些判斷
        //whichroom(player)
        //explode(player)
        //nailtrap(player) (nail也改為用陣列存)
        //holetrap(player)
        //icetrap(player)
        //SetBoom(player)
        //icetrap(player)
        //icetrap(player)
        //CurrentOn(player)
        //CurrentOff(player)
        //ThrowBanana(player)
        //TakeItem(player) 有加撿鑰匙
        //UseKey1(player)
        //UseKey2(player)

var little = 10;
var txt_height = 20;
var limit = 10;
var linecolor =  0xff8800;
var speed = 450;

var batteryNumber; 
var tool1Number;       
var boomNumber;
var bananaNumber;
var key1Number;
var key2Number;

var SlideV=600;//踩到香蕉后的滑行速度
var default_BananaSlideV=600;
var default_BananaSlideA=15; 

var text;
var lastmove = false;
var room_info;

//0618 存物品位置相關
var box_up = 25;
var tool_up = box_up + 5;
var battery_up = tool_up + 8;
var bomb_safe_up = battery_up + 14;
var bomb_danger_up = bomb_safe_up + 14;
var banana_up = bomb_danger_up + 16;
var key_true_up = banana_up + 1;
var key_false_up = key_true_up + 2;

var playState = {
    preload:function(){
        batteryNumber=document.getElementById("batteryNumber");    //获取html中对应的元素，通过更改元素内html内容，来更新背包中物品数目 
        tool1Number=document.getElementById('tool1Number');        //同上
        boomNumber=document.getElementById('boomNumber');          //同上
        bananaNumber=document.getElementById('bananaNumber');      //同上
        key1Number=document.getElementById('key1Number');
        key2Number=document.getElementById('key2Number');

        this.pubnub = new PubNub({
            publishKey : 'pub-c-6bcfcf82-6113-40d7-988b-f70fc3841dc9',
            subscribeKey : 'sub-c-aa0fecec-6315-11e8-8fc9-4adc6d6a94eb'
        });
        this.channels = game.global.room_name + ' player ';
        this.player_channel = this.channels;
        this.player_channel += game.global.my_number;
        console.log(game.global.my_number+" "+this.player_channel);
    },
    create: function(){
        game.stage.backgroundColor = '#000000'; 
        this.inf_ref = firebase.database().ref("game_information/" + game.global.room_name.split('/')[1]);
        //chat
        this.userref = firebase.database().ref(game.global.room_name+"/"+user_name);
        this.talking = false;
        this.talk_to = "";
        this.talk_to_ref = "";
        this.talk_box_txt = "";
        this.talk_box = 0;
        this.message_array = [ 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0];
        this.player_array = [ 0 , 0 , 0 , 0];

        this.nail_1 = [];
        this.nail_2 = [];
        this.CHEST = new Array(81);
        //determine trap event
        this.movable = 1;
        this.bananaTriggered=0; //0617 update: 滑倒相關

        this.create_map();
        this.setchat();
        this.setkeys();
        this.setitems();
        this.setplayers();
        //20180609
        this.create_chest();
        this.create_message();
        //0617 update電流初始
        this.Initial_current();

        this.listen_to_pubnub();
        this.pubnub.subscribe({
            channels: [ this.channels+'0', this.channels+'1', this.channels+'2', this.channels+'3']
        });

        this.userref.child("message").orderByChild("createdAt").on("value",playState.get_message);
    },
    update: function() {
        this.check_time();
        //item_effect
        //蔡
        var i;
        for(i=0;i<81;i++)
        {
            game.physics.arcade.collide(this.player, this.CHEST[i]);
            game.physics.arcade.collide(this.bananas, this.CHEST[i]); //0617 update
        } 
        for(i=0;i<this.nail_1.length;i++){
            game.physics.arcade.collide(this.player, this.nail_1[i],this.death, null, this);
            game.physics.arcade.collide(this.player, this.nail_2[i],this.death, null, this);
            game.physics.arcade.collide(this.nail_1[i], this.nail_2[i],this.nail_end,null,this); //nail_1,nail_2 collide
            if(!this.nail_1[i].alive){
                this.nail_1[i].destroy();
                this.nail_2[i].destroy();
                this.nail_1.splice(i,1);
                this.nail_2.splice(i,1);
            }
        }
        //徐
        //0617 update: 改為和groups collide
        game.physics.arcade.collide(this.player,this.bombs,this.detect_bomb, null, this);
        
        game.physics.arcade.collide(this.player,this.machine1s);      //6.15----------------------------
        game.physics.arcade.collide(this.player,this.machine2s);      //6.15-----------------------------
        game.physics.arcade.collide(this.player,this.guards);
        game.physics.arcade.collide(this.player,this.batteries);
        game.physics.arcade.collide(this.player,this.tools);

        if(!this.bananaTriggered)//添加 与香蕉皮重合时执行滑动效果
            game.physics.arcade.collide(this.player,this.bananas,this.sent_slideEffect, null, this);  
        game.physics.arcade.collide(this.bananas, this.layer);
	    game.physics.arcade.collide(this.bananas, this.boxs);
	    game.physics.arcade.collide(this.bananas, this.machine1s);
	    game.physics.arcade.collide(this.bananas, this.machine2s);
        //6.15------------------------------------------ 删掉了原来的currentOn判断条件
        game.physics.arcade.collide(this.player,this.electrics,this.shockEffect, null, this);          
         //------------------------------------------ 
        //map
        //0617 updates:詹的神奇collide順序 for 箱子
        this.bananas.setAll('body.drag',{x:500,y:500});
        if(this.bananaTriggered) this.player.body.drag.setTo(500,500);

	    game.physics.arcade.collide(this.player,this.boxs,this.boxEffect,null,this);
	    game.physics.arcade.collide(this.player, this.layer);
	    this.movePlayer();
	
	
	    game.physics.arcade.collide(this.boxs, this.machine1s);
        game.physics.arcade.collide(this.boxs, this.machine2s);
        game.physics.arcade.collide(this.boxs, this.layer);
        game.physics.arcade.collide(this.boxs, this.boxlayer);
        for(i=0;i<81;i++)
        {
            game.physics.arcade.collide(this.boxs, this.CHEST[i]);
        } 

        game.physics.arcade.collide(this.player,this.boxs);
        
        for(var i=0;i<25;i++){
            var pos = game.global.object_pos[i];
            this.boxsArray[i].body.x = pos.x;
            this.boxsArray[i].body.y = pos.y;
        }
        //徐
        //0617 update:背包       
        batteryNumber.innerHTML=this.player.battery;             //背包中更新电池数目
        tool1Number.innerHTML=this.player.tool1;                 //背包中更新扳手数目
        boomNumber.innerHTML=this.player.boom;                   //背包中更新炸弹数目
        bananaNumber.innerHTML=this.player.banana;               //背包中更新香蕉数目                                      //添加扔香蕉皮技能
        key1Number.innerHTML=this.player.key1;                   //背包中更新鑰匙数目
        key2Number.innerHTML=this.player.key2;
        //蔡
        this.showroom(); //0617 update:背包
        this.gameOver();
    },
    setkeys: function(){
        //chat
        var keyt = game.input.keyboard.addKey(Phaser.Keyboard.T);//talk
        keyt.onDown.add(this.change_mode, this); 
        var keyy = game.input.keyboard.addKey(Phaser.Keyboard.Y);//sent
        keyy.onDown.add(this.sent_message, this); 
        //item_effect
        //蔡  
        //0617 update: 按鍵功能重設  
        var KEY_SPACE = game.input.keyboard.addKey(Phaser.Keyboard.SPACEBAR); 
        KEY_SPACE.onDown.add(this.checkBox, this);

        var KEY_C =  game.input.keyboard.addKey(Phaser.Keyboard.C);//拿東西按C
        KEY_C.onDown.add(function(){this.using_item('TakeItem');},this);
        var KEY_V =  game.input.keyboard.addKey(Phaser.Keyboard.V);//用電池按V
        KEY_V.onDown.add(function(){this.using_item('CurrentOn');},this);
        var KEY_B =  game.input.keyboard.addKey(Phaser.Keyboard.B);//用工具按B
        KEY_B.onDown.add(function(){this.using_item('CurrentOff');},this); 
        var KEY_N =  game.input.keyboard.addKey(Phaser.Keyboard.N);//放炸彈按N
        KEY_N.onDown.add(function(){this.using_item('SetBoom');},this); 
        var KEY_M =  game.input.keyboard.addKey(Phaser.Keyboard.M);//放香蕉皮按M
        KEY_M.onDown.add(function(){this.using_item('ThrowBanana');},this); 
        var KEY_COMMA =  game.input.keyboard.addKey(Phaser.Keyboard.COMMA);//使用鑰匙1按 '，'
        KEY_COMMA.onDown.add(function(){this.using_item('UseKey1');},this); 
        var KEY_PERIOD =  game.input.keyboard.addKey(Phaser.Keyboard.PERIOD);//使用鑰匙2按 '.'
        KEY_PERIOD.onDown.add(function(){this.using_item('UseKey2');},this); 

        //chat
        this.key1 = game.input.keyboard.addKey(Phaser.Keyboard.ONE);
        this.key1.onDown.add(function() {this.using_number_key(1);}, this); 
        this.key2 = game.input.keyboard.addKey(Phaser.Keyboard.TWO);
        this.key2.onDown.add(function() {this.using_number_key(2);}, this); 
        this.key3 = game.input.keyboard.addKey(Phaser.Keyboard.THREE);
        this.key3.onDown.add(function() {this.using_number_key(3);}, this); 
        this.key4 = game.input.keyboard.addKey(Phaser.Keyboard.FOUR);
        this.key4.onDown.add(function() {this.using_number_key(4);}, this); 
        this.key5 = game.input.keyboard.addKey(Phaser.Keyboard.FIVE);
        this.key5.onDown.add(function() {this.using_number_key(5);}, this); 
        this.key6 = game.input.keyboard.addKey(Phaser.Keyboard.SIX);
        this.key6.onDown.add(function() {this.using_number_key(6);}, this); 
        this.key7 = game.input.keyboard.addKey(Phaser.Keyboard.SEVEN);
        this.key7.onDown.add(function() {this.using_number_key(7);}, this); 
        this.key8 = game.input.keyboard.addKey(Phaser.Keyboard.EIGHT);
        this.key8.onDown.add(function() {this.using_number_key(8);}, this); 
        
        //key inputs
        this.cursor =game.input.keyboard.createCursorKeys();
    },
    using_number_key: function(number_key){
        if(this.talking){
            if( number_key < 5 ){
                if( number_key - 1 != game.global.my_number ){
                    if(game.global.player[number_key-1]){
                        this.talk_to = game.global.player[number_key-1].name;
                        this.talk_box_txt = "To " + this.talk_to + " : ";
                    }
                }
            }
            else{
                var message;
                switch(number_key){
                    case 5: message = "wow"; break;
                    case 6: message = "ya"; break;
                    case 7: message = "hello"; break;
                    case 8: message = "nooooooo"; break;
                }
                if(this.talk_box_txt) this.talk_box_txt = this.talk_box_txt.split(':')[0] + ": " + message;
            }
            this.reset_talk_box();
        }
    },
    using_item: function(item){
        var publishConfig = {
            channel : this.player_channel,
            message: {
                title: "using_item",
                description: item
            }
        };
        this.pubnub.publish(publishConfig);
    },
    setplayers: function(){
        //player create
        for (var i = 0; i < game.global.number_of_player; i++){
            this.player_array[i] = game.add.sprite(game.global.player[i].x, game.global.player[i].y, 'player');
            this.player_array[i].anchor.setTo(0.5, 0.5);
            this.player_array[i].direction = 1;
            this.player_array[i].boom = game.global.player[i].boom;
            this.player_array[i].battery = game.global.player[i].battery;
            this.player_array[i].tool1 = game.global.player[i].tool1;
            this.player_array[i].banana = game.global.player[i].banana;
            this.player_array[i].key1 = game.global.player[i].key1;
            this.player_array[i].key2 = game.global.player[i].key2;
            game.physics.arcade.enable(this.player_array[i]);
            this.player_array[i].body.setSize(32, 32, 8, 8);
            this.player_array[i].body.collideWorldBounds = true;

            this.player_array[i].animations.add('downwalk',  [ 7, 8, 7, 9], 8, true);
            this.player_array[i].animations.add('leftwalk',  [ 0, 1, 2, 3], 8, true);
            this.player_array[i].animations.add('rightwalk', [12,13,14,15], 8, true);
            this.player_array[i].animations.add('upwalk',    [19,20,19,21], 8, true);
            this.player_array[i].animations.add('dead', [29],  8, false);
        }
        this.player = this.player_array[game.global.my_number];
        // 鏡頭跟隨玩家
        this.game.camera.follow(this.player, Phaser.Camera.FOLLOW_LOCKON, 0.1, 0.1);
    },
    movePlayer: function() {
        var is_move = false;

        var horizon = this.cursor.right.isDown - this.cursor.left.isDown ;
        var vertical = this.cursor.down.isDown - this.cursor.up.isDown;

        //0617update: 滑倒相關
        if(!this.bananaTriggered){
            this.player.body.velocity.x = 0
            this.player.body.velocity.y = 0
        }
        if( this.movable && ( horizon || vertical ) ) {
            this.player.body.velocity.x = horizon*speed;
            this.player.body.velocity.y = vertical*speed;
            is_move = true;
            if( vertical ){
                var publishConfig = {
                    channel : this.player_channel,
                    message: {
                        title: "move",
                        description: ( vertical < 0 ) ? "up" : "down",
                        x: this.player.x,
                        y: this.player.y
                    }
                };
                this.pubnub.publish(publishConfig);
            }
            else {
                var publishConfig = {
                    channel : this.player_channel,
                    message: {
                        title: "move",
                        description: ( horizon < 0 ) ? "left" : "right",
                        x: this.player.x,
                        y: this.player.y
                    }
                };
                this.pubnub.publish(publishConfig);
            }
        }  
        // If player is not walking
        if(!is_move) {
            if(lastmove){
                var publishConfig = {
                    channel : this.player_channel,
                    message: {
                        title: "stop",
                        description: this.player.direction,
                        x: this.player.x,
                        y: this.player.y
                    }
                };
                this.pubnub.publish(publishConfig);
            }
        }
        lastmove = is_move; 
        //記在firebase
        this.userref.child("public").update({
            'x': this.player.x,
            'y': this.player.y
        });
    },
    //map
    create_map: function(){
        // 新增地圖
        this.map = this.game.add.tilemap('map')
    
        // 新增圖塊 addTilesetImage( '圖塊名稱' , 'load 時png的命名' )
        this.map.addTilesetImage('wall', 'tile1')
        this.map.addTilesetImage('chest', 'tile2')
    
        // 建立圖層 (圖層名稱為 tile 中所設定)
        this.layer = this.map.createLayer('layer')
        this.layer.resizeWorld()
        //0617 upadte: added another layer
        this.boxlayer= this.map.createLayer('boxlayer')
        this.boxlayer.resizeWorld();
        this.boxlayer.alpha=0
        //0617 upadte: collision相關
        //collide
        this.map.setCollision(592);
        this.map.setCollision(668);
        this.map.setCollision(670);
        this.map.setCollision(632);
        this.map.setCollision(665);
        this.map.setCollision(672);
        this.map.setCollision(674);
        this.map.setCollision(707);
        this.map.setCollision(711);
        this.map.setCollision(713);
        this.map.setCollision(717);
        this.map.setCollision(721);
        this.map.setCollision(1186);

        this.map.setCollision(4,true,'boxlayer');//0617 upadte

        this.map.setTileIndexCallback(680, this.disappear, this);
        this.map.setTileIndexCallback([94,203,593,863], this.appear, this);
    },
    create_chest: function(){
        for(i=0;i<81;i++)
        {   
            if(game.global.chest_events[i]){
                var chest_pos=this.getChestPosition(i);
                this.CHEST[i]=game.add.sprite(chest_pos.x, chest_pos.y, 'chest');
                this.CHEST[i].anchor.setTo(0.5, 0.5);
                this.CHEST[i].height=64; 
                game.physics.arcade.enable(this.CHEST[i]);
                this.CHEST[i].body.immovable = true;
                this.CHEST[i].animations.add('open',[54,66,78,90],2,false)
                this.CHEST[i].frame=54;
            }
        } 
    },
    create_message: function(){
        //訊息+訊息框
        text = game.add.text(3*game.width/4, game.height-40, 
            "", { font: '25px Arial', fill: '#ff0000' });
        text.anchor.setTo(0.5,0.5);
        text.fixedToCamera = true;
        var g = game.add.graphics(0, 0);        
        g.lineStyle(2, linecolor, 1);
        g.drawRoundedRect(game.width/2, game.height-6*little-txt_height,game.width/2-10, 5*little+txt_height, 10);
        g.fixedToCamera = true;
        //房號
        room_info= game.add.text(game.width-55, 25, 
            "", { font: '25px Arial', fill: '#ffff00' });
        room_info.anchor.setTo(0.5,0.5);
        room_info.fixedToCamera = true;
    },
    //房間右跟下的門屬於這房間，左跟上的門屬於其他房間
    //range:0~80
    whichroom:function(player){
        return Math.floor((player.y-207)/640)*9+Math.floor((player.x-79)/640) ;   
    },
    //輸入房間號碼,回傳箱子座標
    getChestPosition:function(num){
        var pos={
            x:288+(num%9)*640,
            y:416+(Math.floor(num/9))*640
        }
        return pos;
    },
    //chat
    setchat: function(){
        //聊天視窗
        this.graphics = game.add.graphics(0, 0);
        this.graphics.lineStyle(2, linecolor, 1);
        this.graphics.drawRoundedRect(little, game.height-3*little-txt_height,
                                      game.width/2-2*little, 2*little+txt_height, 10);
        this.graphics.fixedToCamera = true;

        //訊息視窗
        var g = game.add.graphics(0, 0);
        g.lineStyle(2, linecolor, 1);
        g.drawRect(2*little, game.height-2*little-txt_height, game.width/2-4*little,txt_height);
        g.fixedToCamera = true;
    },
    change_mode: function(){
        var i;
        this.talking = !this.talking;
        this.graphics.destroy();
        this.graphics = game.add.graphics(0, 0);
        this.graphics.lineStyle(2, linecolor, 1);
        this.graphics.fixedToCamera = true;
        if(this.talking){
            this.graphics.drawRoundedRect(little, game.height/2+little,
                                          game.width/2-2*little, game.height/2-2*little, 10);
            for(i=0;i<limit;i++) if(this.message_array[i]) this.message_array[i].visible = true;
        }
        else {
            this.graphics.drawRoundedRect(little, game.height-3*little-txt_height,
                                          game.width/2-2*little, 2*little+txt_height, 10);
            for(i=0;i<limit;i++) if(this.message_array[i]) this.message_array[i].visible = false;
        }
    },
    get_message: function(snapshot) {
        var num = snapshot.numChildren();
        var i = 0;
        snapshot.forEach(function(childSnapshot) {
            if( num-i > limit ) 
            firebase.database().ref(game.global.room_name+"/"+user_name+
                                    "/message/"+childSnapshot.key).remove();
            else{
                var childData = childSnapshot.val();
                var txt = ( ( childData.player==user_name ) ? "" : childData.player + " : ") + childData.text;
                if(playState.message_array[i]) playState.message_array[i].destroy();
                playState.message_array[i] = game.add.text( 2*little , game.height-2*little-txt_height*(num-i+2)+3,
                                                            txt , { font: '18px Arial', fill: '#ffffff' });
                playState.message_array[i].fixedToCamera = true;
                if(!playState.talking){
                    playState.message_array[i].visible = false;
                }
            }
            i++;
        });
    },
    sent_message: function(){
        if(this.talk_box_txt){
            currenttime = firebase.database.ServerValue.TIMESTAMP;
            this.talk_to_ref = firebase.database().ref(game.global.room_name + "/" + 
                                                        this.talk_to + "/message");
            var talk_box_txt = this.talk_box_txt;
            this.talk_to_ref.push().set({
                'player': user_name,
                'text': talk_box_txt.split(':')[1],
                'createdAt': currenttime
            })
            this.userref.child("message").push().set({
                'player': user_name,
                'text': talk_box_txt,
                'createdAt': currenttime
            })
            this.talk_box_txt = this.talk_box_txt.split(':')[0] + ": ";
            this.reset_talk_box();
        }
    },
    reset_talk_box: function(){
        if(this.talk_box) this.talk_box.destroy();
        this.talk_box = game.add.text(2*little, game.height-2*little-txt_height+3,
                                      this.talk_box_txt, { font: '18px Arial', fill: '#ffffff' });     
        this.talk_box.fixedToCamera = true;
    },
    //以下皆為item_effect
    setitems: function(){
       //machine create--------------------------------------------------------------------------- 
        if(1){                                        
            this.machine1Array=new Array(4);//生成4组电机
            this.machine2Array=new Array(4);
            this.machine1s=game.add.group(); 
            this.machine2s=game.add.group(); 
    
            this.machine1s.enableBody = true; 
            this.machine2s.enableBody = true;
            this.machinePositions=new Array(4); 
            this.machinePositions[0]=this.getChestPosition(21);
            this.machinePositions[1]=this.getChestPosition(23);
            this.machinePositions[2]=this.getChestPosition(29);
            this.machinePositions[3]=this.getChestPosition(47);
            
            for (var i=0;i<2;i++)                                     //垂直电流
            {
            this.machine1Array[i]=game.add.sprite(this.machinePositions[i].x+192,this.machinePositions[i].y+192,'machine',0,this.machine1s);
            this.machine2Array[i]=game.add.sprite(this.machinePositions[i].x+192,this.machinePositions[i].y+2368,'machine',0,this.machine2s);
            this.machine1Array[i].power=true;
            this.machine2Array[i].power=true;
            this.machine1Array[i].tint=0xff00ff;
            this.machine2Array[i].tint=0xff00ff;
            this.machine1Array[i].anchor.setTo(0.5,0.5);
            this.machine2Array[i].anchor.setTo(0.5,0.5);
            }
            for (var i=2;i<4;i++)                                     //水平电流               
            {
            this.machine1Array[i]=game.add.sprite(this.machinePositions[i].x+192,this.machinePositions[i].y+192,'machine',0,this.machine1s);
            this.machine2Array[i]=game.add.sprite(this.machinePositions[i].x+2368,this.machinePositions[i].y+192,'machine',0,this.machine2s);
            this.machine1Array[i].power=true;
            this.machine2Array[i].power=true;
            this.machine1Array[i].tint=0xff00ff;
            this.machine2Array[i].tint=0xff00ff;
            this.machine1Array[i].anchor.setTo(0.5,0.5);
            this.machine2Array[i].anchor.setTo(0.5,0.5);
            }
            this.machine1s.setAll('body.immovable', true);
            this.machine2s.setAll('body.immovable', true);  
        }   
        //guard-------------------------------------------------------------------------------------------
        if(1){
            var exit=this.getChestPosition(40);
            this.guardArray=new Array(4);
            this.guards=game.add.group();
    
            this.guardArray[0]=game.add.sprite(exit.x,exit.y+64*6,'guard'); //down
            this.guardArray[0].frame=0;
            this.guardArray[1]=game.add.sprite(exit.x-64*6,exit.y,'guard'); //left
            this.guardArray[1].frame=4;
            this.guardArray[2]=game.add.sprite(exit.x+64*6,exit.y,'guard'); //right
            this.guardArray[2].frame=8;
            this.guardArray[3]=game.add.sprite(exit.x,exit.y-64*6,'guard'); //up
            this.guardArray[3].frame=12;
    
            for (var i=0;i<4;i++){
                this.guards.add(this.guardArray[i]);
                this.guardArray[i].anchor.setTo(0.5,0.5);
                game.physics.arcade.enable(this.guardArray[i]);            
            } 
            this.guards.setAll('body.immovable', true);
        }
        //0614 update:宣告 group
        this.bananas= game.add.group();
        this.bombs  = game.add.group();
        this.batteries = game.add.group();
        this.tools = game.add.group();//0618
        this.keys = game.add.group();

        this.electrics = game.add.group(); //6.15--------------加入了电流的group
        this.electricArray= new Array(4);
        
        //箱子---------------------------------------------------------------------------------
        this.boxs=game.add.group(); 
        this.boxs.enableBody = true; 
        this.boxsArray= new Array(25);
        //0617 更新已有的東西
        for (var i=0;i<85;i++){//25+5+8+16+14+14
            if(game.global.object_pos[i]){
                if( i < box_up ){
                    var pos = game.global.object_pos[i]
                    this.boxsArray[i]=game.add.sprite(pos.x,pos.y,'box',0,this.boxs); 
                }
                else if( i < tool_up ) this.show_item("tool",game.global.object_pos[i]);
                else if( i < battery_up ) this.show_item("battery",game.global.object_pos[i]);
                else if( i < bomb_safe_up ) this.show_item("bomb",game.global.object_pos[i]);
                else if( i < bomb_danger_up ){
                    var newbomb = game.add.sprite(x,y, 'boom');
                    newbomb.anchor.setTo(0.5,0.5);
                    this.game.physics.arcade.enable(newbomb);
                    newbomb.body.immovable = true;
                    newbomb.frame=0;
                    newbomb.safe=false;
                    newbomb.alpha=0;
                    newbomb.body.setSize(22, 30, 5, 35);
                    this.bombs.add(newbomb);
                }
                else if( i < banana_up ) this.show_item("banana",game.global.object_pos[i]);
                else if( i < key_true_up ) this.show_item("key_true",game.global.object_pos[i]);
                else if( i < key_false_up ) this.show_item("key_false",game.global.object_pos[i]);
            }
        }
        this.boxs.setAll('body.immovable', false);
    },
    checkBox: function(){
        //各房間開啟寶箱+event 觸發
        var room = this.whichroom(this.player);
        var pos= this.getChestPosition(room);
        if(this.player.y<(pos.y+60)&& this.player.y>pos.y&&this.player.x<(pos.x+32)&&this.player.x>pos.x-32&&this.CHEST[room])
        {
            if(this.CHEST[room].alive){
                this.CHEST[room].alive = false;
                if(room==40){
                    var publishConfig = {
                        channel : this.player_channel,
                        message: {
                            title: "game_end",
                        }
                    };
                    this.pubnub.publish(publishConfig);
                    return;
                }
                else{
                    var publishConfig = {
                        channel : this.player_channel,
                        message: {
                            title: "chest_open",
                        }
                    };
                    this.pubnub.publish(publishConfig);
                }
            }
        }
    },
    chest_open: function(player){
        var room = this.whichroom(player);
        var pos= this.getChestPosition(room);
        this.CHEST[room].alive = false;
        this.CHEST[room].animations.play('open');
        console.log(room+" "+this.CHEST[room]+" "+game.global.chest_events[room]);
        if( player == this.player ){
            this.movable = 0;
            game.camera.flash(0xffffff, 3000);
        }
        game.time.events.add(3000, function() {
            console.log(room+" "+this.CHEST[room]+" "+game.global.chest_events[room]);
            this.CHEST[room].kill();
            if( player == this.player ){
                if(game.global.hints.some(function(currentValue){return (currentValue == room);})) this.hint(room);
                var chest_event = game.global.chest_events[room];
                this.movable = 1;
                var publishConfig = {
                    channel : this.player_channel,
                    message: {
                        title: "chest_event",
                        description: chest_event,
                        other: pos
                    }
                };
                this.pubnub.publish(publishConfig);
                game.global.chest_events[room] = "";//disappear
                this.inf_ref.update({
                    chest_events: game.global.chest_events
                });
            }
        },this);
    },
    hint: function(room){
        var i,j;
        for(i=-2;i<=2;i++) for(j=-2;j<=2;j++){
            if( i == 0 && j == 0 ) continue;
            var check_room = room+i*9+j;
            if( check_room%9 == 0 && j > 0 ) continue;
            if( check_room%9 == 8 && j < 0 ) continue;
            if( check_room%9 == 7 && j == -2 ) continue;
            if( check_room%9 == 1 && j == 2 ) continue;
            if( 0 <= check_room && check_room <= 80) {
                var chest_event = game.global.chest_events[check_room];
                if( chest_event=="key_true" || chest_event=="key_false" ) break;
            }
        }
        if(i==3) text.setText("There is no key near you");    //新增text
        else text.setText("There may be some keys near you");    //新增text
        game.time.events.add(2000, function() {
            text.setText('');
        },this);
    },
    explode: function(player){
        var bomb = game.add.sprite(player.x, player.y, 'explosion');
        bomb.anchor.setTo(0.5,0.5);
        bomb.width=200;
        bomb.height=200;
        bomb.animations.add('BOOM',  [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14], 8, false);
        bomb.animations.play('BOOM');
        if( player == this.player ){
            this.movable=0;
            game.camera.shake(0.02, 2100);
            text.setText("You step on the BOMB!!");    //新增text
        }
        game.time.events.add(2000, function() {
            text.setText('');
            bomb.destroy();
            if( player == this.player ) this.death();
        },this);
    },
    nailtrap: function(player){
        //nail create 改到這
        var room = this.whichroom(player);
        var pos = this.getChestPosition(room);
        var number = this.nail_1.length;

        this.nail_1[number] = game.add.sprite(pos.x,pos.y-64*3,'nail1');
        this.nail_1[number].width=448;
        this.nail_1[number].anchor.setTo(0.5,0.5);
        game.physics.arcade.enable(this.nail_1[number]);
        this.nail_1[number].body.immovable = true;
        this.nail_1[number].body.gravity.y=40;

        this.nail_2[number]= game.add.sprite(pos.x,pos.y+64*3,'nail2');
        this.nail_2[number].width=448;
        this.nail_2[number].anchor.setTo(0.5,0.5);
        game.physics.arcade.enable(this.nail_2[number]);
        this.nail_2[number].body.immovable = true;
        this.nail_2[number].body.gravity.y=-40;

        if( room == this.whichroom(this.player) ){
            text.setText('nail trap activated');    //新增text
            //過一段時間,消除text
            game.time.events.add(4500, function() {
                text.setText('');
            },this);
        }
    },
    nail_end:function(nail_1,nail_2){
        nail_1.alive = false;
        nail_2.alive = false;
    },
    //傳送完後須回報新位置
    holetrap: function(player){
        var room = this.whichroom(player);
        var random = Math.floor((Math.random() * 80));  //random 房間
        while( random == 0 || random == 8 || random == 72 ||
               random == 80 || random == 40 || random == room ){
            random = Math.floor((Math.random() * 80));
        }
        var hole = game.add.sprite(player.x, player.y, 'hole');
        hole.anchor.setTo(0.5,0.5);
        game.world.swap(hole, player);
        game.add.tween(hole).to({angle: -180}, 2000).loop().start();
        game.add.tween(player.scale).to({x: 0, y:0}, 1000).start();
        if( player == this.player ){
            this.movable=0;
            text.setText('hole trap activated');    //新增text
            //過一段時間,消除text
            game.time.events.add(1100, function() { 
                text.setText('');
            },this);
        }
        game.time.events.add(1100, function() {
            hole.destroy();
            game.add.tween(player.scale).to({x: 1, y:1}, 1000).start()
            if( player == this.player ){
                this.movable=1;
                this.player.x=this.getChestPosition(random).x;    //random 房間
                this.player.y=this.getChestPosition(random).y+64; //random 房間
                var publishConfig = {
                    channel : this.player_channel,
                    message: {
                        title: "new_pos",
                        x: this.player.x,
                        y: this.player.y
                    }
                };
                this.pubnub.publish(publishConfig);
            }
        },this);
    },
    icetrap: function(player){
        var ice = game.add.sprite(player.x, player.y, 'ice');
        ice.anchor.setTo(0.5,0.5);
        game.world.swap(ice, player);
        ice.width=150;
        ice.height=150;        
        ice.animations.add('frozen',  [16,15,14,13,12,11,10,9,8,7,6,5,4,3,2,1,0], 8, false);
        ice.animations.add('melt',  [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15], 8, false)
        ice.animations.play('frozen');
        if( player == this.player ){
            this.movable=0; 
            text.setText('ice trap activated');   
        }
        //過一段時間,消除text
        game.time.events.add(10000, function() { 
            if( player == this.player ){
                this.movable=1;  
                text.setText('');
            }
            ice.animations.play('melt');          
        },this);
        game.time.events.add(13000, function() { 
            ice.kill();           
        },this);
    },
    death:  function(){
        var publishConfig = {
            channel : this.player_channel,
            message: {
                title: "death"
            }
        };
        this.pubnub.publish(publishConfig);
    },
    gameOver: function(){
        var i,total_alive = 0;
        for(i=0;i<game.global.number_of_player;i++) total_alive += this.player_array[i].alive;
        for(i=0;i<game.global.number_of_player;i++) 
            if(!this.player_array[i].alive){
                this.player_array[i].animations.play('dead');
                if( this.player == this.player_array[i] ){
                    if( !total_alive ) this.inf_ref.remove();
                    if( i == game.global.my_number ) this.userref.remove();
                    game.time.events.add(200, function() {
                        alert("You're DEAD!");
                        changeto_gamemode();
                    },this);
                }
            }
    },
    disappear:function(item){
        var i;
        for(i=0;i<game.global.number_of_player;i++){
            if( item == this.player_array[i] ) item.alpha = 0;
        } 
        for(i=0;i<25;i++){
            if( item == this.boxsArray[i] ) item.alpha = 0;
        } 
    },
    appear: function(item){
        var i;
        for(i=0;i<game.global.number_of_player;i++){
            if( item == this.player_array[i] ) item.alpha = 1;
        } 
        for(i=0;i<25;i++){
            if( item == this.boxsArray[i] ) item.alpha = 1;
        } 
    },
    //徐
    SetBoom: function(player){
        if( player.boom>0 ){
            var x = player.body.position.x;
            var y = player.body.position.y;
            if(player.direction==3) {   //right
                x=x+60;
            }
            else if(player.direction==2) {  //left
                x=x;
            }
            else if(player.direction==1) { //down
                y=y+40;
                x=x+30;
            }
            else{
                y=y-40;  //up
                x=x+30;
            }

            //boom
            var newbomb = game.add.sprite(x,y, 'boom');
            newbomb.anchor.setTo(0.5,0.5);
            this.game.physics.arcade.enable(newbomb);
            newbomb.body.immovable = true;
            newbomb.frame=0;
            newbomb.safe=false;
            newbomb.body.setSize(22, 30, 5, 35);
            this.bombs.add(newbomb);

            var bombs_pos = { x:x, y:y };
            this.update_object_pos("bomb_danger",bombs_pos,0,true,player);
            if( this.player == player){
                var set=game.add.tween(newbomb).to( { alpha: 0 }, 1000, Phaser.Easing.Linear.None).start();
                set.repeat(2);
            }
            //
            player.boom-=1; 
        }  
    },
    //collide effect
    boxEffect: function(player, box){
        box.body.velocity.x = 0;
        box.body.velocity.y = 0;
        box.body.drag.setTo(1500, 1500);
        if(player.direction==3) {
            box.body.velocity.x+=5;
        }
        else if(player.direction==2) {
            box.body.velocity.x-=5;
        }
        else if(player.direction==1) {
            box.body.velocity.y+=5;
        }
        else{
            box.body.velocity.x-=5;
        }

        var i;
        for(i=0;i<25;i++) if(this.boxsArray[i]==box) break;
        var pos = {x:box.body.x, y:box.body.y};
        this.update_object_pos("box", pos, i, true, player);
    },
    boomEffect: function(boom,player){
        if( this.isNear(boom,player) && boom.safe ){
            var boom_pos = { x:boom.body.x, y:boom.body.y };
            this.update_object_pos("bomb_safe",boom_pos,0,false,player);
            boom.kill();
            player.boom+=1;
            console.log(player.boom);  
        }
    },
    detect_bomb: function(player,boom){
        if(boom.safe==false){
            this.explode(player);
            boom.kill();
            boom.body.setSize(0, 0, 0, 0);
        }         
    },
    //0617 update 取代
    find_machine: function(x,y){
        var i,k;
        if(x<2880){
            if(x<1900){
                if(y<2880){i=2;k=1;}
                else{i=3;k=1;}
            }
            else{
                if(y<2880){i=0;k=1;}
                else{i=0;k=2;}
            };
        }
        else{
            if(x<3800){
                if(y<2880){i=1;k=1;}
                else{i=1;k=2;}
            }
            else{
                if(y<2880){i=2;k=2;}
                else{i=3;k=2;}
            }
        }
        var pos = { group:i , num:k };
        return pos;
    },
    CurrentOn:function(player){  
        console.log("press V");
        var x=player.position.x;
        var y=player.position.y;
        var pos = this.find_machine(x,y);
        var i = pos.group, k = pos.num;
        //打開machine1
        if(k==1){
            if(this.isNear(player,this.machine1Array[i]))
            {
                if(this.machine1Array[i].power==false && player.battery > 0 ){
                player.battery-=1;
                this.machine1Array[i].power=true;
                this.machine1Array[i].tint=0xff00ff;
                }   
            }
        }
        //打開machine2
        else if(k==2){
            if(this.isNear(player,this.machine2Array[i]))
            {
                if(this.machine2Array[i].power==false && player.battery > 0 ){
                player.battery-=1;
                this.machine2Array[i].power=true;
                this.machine2Array[i].tint=0xff00ff;
                }   
            }
        }
        //確認通電+播動畫       
        if(!this.electricArray[i].alive){
            if( this.machine1Array[i].power==true && this.machine2Array[i].power==true ){
                if(i<2){
                    this.electricArray[i] = game.add.sprite(this.machinePositions[i].x+192,this.machinePositions[i].y+192+24, 'electricV',0,this.electrics);
                    this.electricArray[i].animations.add('electricV',[0,1,2,3],12,true); //垂直电流
                    this.electricArray[i].animations.play('electricV');
                }
                else{
                    this.electricArray[i] = game.add.sprite(this.machinePositions[i].x+192+23,this.machinePositions[i].y+192, 'electricH',0,this.electrics);
                    this.electricArray[i].animations.add('electricH',[0,1,2,3],12,true);//水平电流
                    this.electricArray[i].animations.play('electricH');
                }
                game.physics.arcade.enable(this.electricArray[i]);
                this.electricArray[i].body.immovable = true;
                this.electricArray[i].alive = true;
            }
        }
    },
    CurrentOff:function(player){                                          
        console.log("press B");
        var x=player.position.x;
        var y=player.position.y;
        var pos = this.find_machine(x,y);
        var i = pos.group, k = pos.num;
        if( ( k==1 && this.isNear(player,this.machine1Array[i]) ) || 
            ( k==2 && this.isNear(player,this.machine2Array[i]) ) ){
            if( this.electricArray[i] && player.tool1>0 ){
                this.electricArray[i].kill();
                this.electricArray[i].alive=0;
                player.tool1-=1;
                this.machine1Array[i].power=false;
                this.machine1Array[i].tint=0xffffff;
                this.machine2Array[i].power=false;
                this.machine2Array[i].tint=0xffffff;
            }
        }
    },
    batteryEffect: function(battery, player){
        if(this.isNear(battery,player)){ 
            var battery_pos = { x:battery.body.x, y:battery.body.y };
            this.update_object_pos("battery",battery_pos,0,false,player);
            battery.kill();
            player.battery+=1;
            console.log(player.battery);
        }        
    },
    shockEffect: function(){
        game.camera.shake(0.02, 300);
        game.camera.flash(0xffffff, 300);
        this.death();
    },
    tool1Effect: function(tool1,player){
        if(this.isNear(tool1,player)){ 
            var tool_pos = { x:tool1.body.x, y:tool1.body.y };
            this.update_object_pos("tool",tool_pos,0,false,player);
            tool1.kill();
            player.tool1+=1;
            console.log(player.tool1);
        }
    },
    keyEffect: function(key,player){
        if(this.isNear(key,player)){ 
            var key_pos = { x:key.body.x, y:key.body.y };
            if( key.type == game.global.true_key ) this.update_object_pos("key_true",key_pos,0,false,player);
            else this.update_object_pos("key_false",key_pos,0,false,player);
            if( key.type == 1 ) player.key1+=1;
            else player.key2+=1;
            key.kill();
        }
    },
    //0617 update: 香蕉相關
    sent_slideEffect: function(player,banana){            //踩到香蕉皮滑动效果
        if(this.bananaTriggered==0){
            var publishConfig = {
                channel : this.player_channel,
                message: {
                    title: "slideEffect",
                    description: player.direction,
                    x: player.x,
                    y: player.y,
                    banana_x: banana.x,
                    banana_y: banana.y,
                }
            };
            this.pubnub.publish(publishConfig);
        }
        this.bananaTriggered=1;
    },
            
    slideEffect: function(player,banana){
        if( player == this.player ){
            this.movable=0;
            this.bananaTriggered=1;
        }
        if(player.direction==0){
            player.body.velocity.y=-1*SlideV;
            banana.body.velocity.y=-1*SlideV;
            player.direction=0;
        }
        else if(player.direction==1){
            player.body.velocity.y=SlideV;
            banana.body.velocity.y=SlideV;
            player.direction=1;
        }
        else if(player.direction==2){
            player.body.velocity.x=-1*SlideV;
            banana.body.velocity.x=-1*SlideV;
            player.direction=2;
        }
        else if(player.direction==3){
            player.body.velocity.x=SlideV;
            banana.body.velocity.x=SlideV;
            player.direction=3;
        }           
        var banana_pos = { x:banana.body.x, y:banana.body.y };

        game.time.events.add(1500,function(){
            banana.destroy();
            player.banana+=1;
            this.update_object_pos("banana",banana_pos,0,false,player);
            console.log('banana '+player.banana);
            SlideV=default_BananaSlideV;
            this.movable=1;
            this.bananaTriggered=0;	
        },this);
    },
    ThrowBanana: function(player){                         //扔出香蕉皮效果
        if(player.banana>0){
            var x = player.body.position.x;
            var y = player.body.position.y;
            if(player.direction==3) {
                x=x+40;
            }else if(player.direction==2) {
               x=x-40;
            }else if(player.direction==1) {          
                y=y+40;
            }else{
                y=y-40;
            }
            var newbanana = game.add.sprite(x,y, 'banana');
            this.game.physics.arcade.enable(newbanana);
            newbanana.body.immovable = true;
            this.bananas.add(newbanana);
            player.banana-=1;
            var pos = { x:x, y:y };
            this.update_object_pos("banana",pos,0,true,player);
            console.log(player.banana);  
        }  
    },

    //downcounter
    check_time: function () {
        var currenttime = Date.now();
        var left_time = time_limit - Math.round((currenttime - game.global.start_time) / 1000);
        if ( left_time >= 0 ) {
            game.debug.text(this.formatTime(left_time), 2, 14, "#ff0");
        }
        else {
            this.player.alive = false;
        }
    },
    formatTime: function(s) {
        var minutes = "0" + Math.floor(s / 60);
        var seconds = "0" + (s - minutes * 60);
        return minutes.substr(-2) + ":" + seconds.substr(-2);   
    },
    //0617 update: 看東西有沒有在旁邊 
    isNear: function(item1,item2){
        if( (item1.body.position.x-item2.body.position.x>-60) &&
            (item1.body.position.x-item2.body.position.x< 60) &&
            (item1.body.position.y-item2.body.position.y>-50) &&
            (item1.body.position.y-item2.body.position.y< 50) ){
            return 1;
        }
        else return 0;
    },
    //0617 update: 看東西有沒有在旁邊，有的化撿起來
    TakeItem: function(player){
        this.batteries.forEach(this.batteryEffect,this,true,player);
        this.tools.forEach(this.tool1Effect,this,true,player);
        this.bombs.forEach(this.boomEffect,this,true,player);
        this.keys.forEach(this.keyEffect,this,true,player);
    },
    //0617 update: 初始化電流
    Initial_current: function(){
        for (var i=0;i<2;i++){ 
            this.electricArray[i] = game.add.sprite(this.machinePositions[i].x+192,this.machinePositions[i].y+192+24, 'electricV',0,this.electrics);
            this.electricArray[i].animations.add('electricV',[0,1,2,3],12,true); //垂直电流
            this.electricArray[i].animations.play('electricV');
            this.electricArray[i].alive=1;
            game.physics.arcade.enable(this.electricArray[i]);
            this.electricArray[i].body.immovable = true;
        }
        for (var i=2;i<4;i++){ 
            this.electricArray[i] = game.add.sprite(this.machinePositions[i].x+192+23,this.machinePositions[i].y+192, 'electricH',0,this.electrics);
            this.electricArray[i].animations.add('electricH',[0,1,2,3],12,true);//水平电流
            this.electricArray[i].animations.play('electricH');
            this.electricArray[i].alive=1;
            game.physics.arcade.enable(this.electricArray[i]);
            this.electricArray[i].body.immovable = true;
        }
    },
    //0617 update: 顯示目前房間
    showroom: function(){
        var room_number = this.whichroom(this.player)+1;
        room_info.setText('Room:'+ room_number);
    },
    //0617 update: simple keycheck
    UseKey1: function(player){
        if(player.key1) this.guards.forEach(this.CheckKey,this,true,player,1);
    },
    UseKey2: function(player){
        if(player.key2) this.guards.forEach(this.CheckKey,this,true,player,2);
    },
    CheckKey: function(guard,player,keyid){
        if(this.isNear(guard,player)){
            if( keyid == 1 ) player.key1--;
            else player.key2--;
            if( keyid == game.global.true_key ) guard.kill();
            else this.explode(player);
        }
    },
    //多人
    listen_to_pubnub: function(){
        this.pubnub.addListener({
            message: function(msg) {//msg 目前只做move、stop
                var player = playState.player_array[msg.channel.split(' ')[2]];
                var is_me = (msg.channel.split(' ')[2] == game.global.my_number);
                if( msg.message.title == "move" || msg.message.title == "stop" || 
                    msg.message.title == "new_pos" || msg.message.title == "slideEffect" ){
                    if( !is_me ) {
                        player.x = msg.message.x;
                        player.y = msg.message.y;
                    }
                }
                if( msg.message.title == "move" ){
                    if( msg.message.description == "left" ) player.direction = 2;
                    if( msg.message.description == "right" ) player.direction = 3;
                    if( msg.message.description == "up" ) player.direction = 0;
                    if( msg.message.description == "down" ) player.direction = 1;
                    switch(player.direction) {
                        case 0: player.animations.play('upwalk'); break;
                        case 1: player.animations.play('downwalk'); break;
                        case 2: player.animations.play('leftwalk'); break;
                        case 3: player.animations.play('rightwalk'); break;
                    }   
                }
                else if( msg.message.title == "stop" ){
                    switch(msg.message.description) {
                        case 0: player.frame=19; break;
                        case 1: player.frame=7; break;
                        case 2: player.frame=0; break;
                        case 3: player.frame=12; break;
                    }
                    player.animations.stop();
                }
                else if( msg.message.title == "chest_event" ){
                    if( msg.message.description == "bomb" ) playState.explode(player);
                    else if( msg.message.description == "nail" ) playState.nailtrap(player);
                    else if( msg.message.description == "hole" ) playState.holetrap(player);
                    else if( msg.message.description == "freeze" ) playState.icetrap(player);
                    else {
                        playState.show_item(msg.message.description,msg.message.other);
                        playState.update_object_pos(msg.message.description,msg.message.other,0,true,player);
                    }
                }
                else if( msg.message.title == "chest_open" ) playState.chest_open(player);
                else if( msg.message.title == "death" ) player.alive = false;
                else if( msg.message.title == "game_end" ) {
                    if( is_me ) alert("YOU WIN");
                    else alert("YOU LOSE");
                }
                else if( msg.message.title == "using_item" ){
                    if( msg.message.description == "TakeItem" ) playState.TakeItem(player);
                    if( msg.message.description == "CurrentOn" ) playState.CurrentOn(player);
                    if( msg.message.description == "CurrentOff" ) playState.CurrentOff(player);
                    if( msg.message.description == "SetBoom" ) playState.SetBoom(player);
                    if( msg.message.description == "ThrowBanana" ) playState.ThrowBanana(player);
                    if( msg.message.description == "UseKey1" ) playState.UseKey1(player);
                    if( msg.message.description == "UseKey2" ) playState.UseKey2(player);
                }
                else if( msg.message.title == "slideEffect" ) {
                    playState.bananas.forEach(this.find_banana,this,true,msg.message.x);
                    playState.slideEffect(player);
                }
                //丟出object時 亦須傳x,y給所有人，再接收訊息
                //丟出object時 同時也須上傳新座標在firebase上
            }
        });
    },
    show_item: function(item, pos){
        //0617 update:關於道具實現方式  增加sprite->設屬性->加入所屬group
        if( item == "boom" )
        {
            var newbomb = game.add.sprite(pos.x,pos.y, 'boom');
            this.game.physics.arcade.enable(newbomb);
            newbomb.body.immovable = true;
            newbomb.frame=0;
            newbomb.safe=true;
            newbomb.anchor.setTo(0.5,0.5);
            newbomb.body.setSize(22, 30, 5, 35);
            this.bombs.add(newbomb);
        }
        else if ( item == "banana" ){       
            var newbanana = game.add.sprite(pos.x,pos.y, 'banana');
            this.game.physics.arcade.enable(newbanana);
            newbanana.body.immovable = true;
            this.bananas.add(newbanana);
        }
        else if ( item == "battery" ){
            var newbattery = game.add.sprite(pos.x,pos.y, 'battery');
            this.game.physics.arcade.enable(newbattery);
            newbattery.body.immovable = true;
            this.batteries.add(newbattery);
        }
        else if ( item == "tool" ){
            var newtool = game.add.sprite(pos.x,pos.y, 'tool1');
            this.game.physics.arcade.enable(newtool);
            newtool.body.immovable = true;
            this.tools.add(newtool);
        }
        else if ( item == "key_true" ){
            var newkey;
            if(game.global.true_key==1) newkey = game.add.sprite(pos.x,pos.y, 'key1');
            else newkey = game.add.sprite(pos.x,pos.y, 'key2');
            this.game.physics.arcade.enable(newkey);
            newkey.body.immovable = true;
            newkey.type = game.global.true_key;
            this.keys.add(newkey);
        }
        else if ( item == "key_false" ){
            var newkey;
            if(game.global.true_key==1) newkey = game.add.sprite(pos.x,pos.y, 'key2');
            else newkey = game.add.sprite(pos.x,pos.y, 'key1');
            this.game.physics.arcade.enable(newkey);
            newkey.body.immovable = true;
            newkey.type = ( game.global.true_key == 1 ) ? 2 : 1;
            this.keys.add(newkey);
        }
    },
    update_object_pos: function(item, pos, index, put, player){//index for box
        var i,n;
        if( item == "tool" ) {i=box_up; n=tool_up; }
        else if( item == "battery" ) {i=tool_up; n=battery_up; }
        else if( item == "bomb_safe" ) {i=battery_up; n=bomb_safe_up; }
        else if( item == "bomb_danger" ) {i=bomb_safe_up; n=bomb_danger_up; }
        else if( item == "banana" ) {i=bomb_danger_up; n=banana_up; }
        else if( item == "key_true" ) {i=banana_up; n=key_true_up; }
        else if( item == "key_false" ) {i=key_true_up; n=key_false_up; }

        if( item == "box" ) {
            game.global.object_pos[index] = pos;
            this.boxsArray[index].body.x = pos.x;
            this.boxsArray[index].body.y = pos.y;
        }
        else if(put){
            for(;i<n;i++) if(!game.global.object_pos[i]) break
            game.global.object_pos[i] = pos;
        }
        else{  
            for(;i<n;i++){
                if( game.global.object_pos[i].x == pos.x && game.global.object_pos[i].y == pos.y ) {
                    game.global.object_pos[i] = "";
                    break;
                }
            }     
        }
        if( player == this.player ) {
            this.inf_ref.update({
                object_pos: game.global.object_pos
            });
            this.userref.child("public").update({
                boom: player.boom,
                battery: player.battery,
                tool1: player.tool1,
                banana: player.banana,
                key1: player.key1,
                key2: player.key2
            })
        }
    },
};