var waitingState = { 
    create: function(){
        this.name_array = [ 0 , 0 , 0 , 0 ]//name不能為0
        this.ready = false;
        this.ref = firebase.database().ref(game.global.room_name);
        this.inf_ref = firebase.database().ref("game_information/" + game.global.room_name.split('/')[1]);
        this.set_current_data();
    },
    set_current_data: function(){
        this.ref.child(user_name+"/public").set({
            'ready': waitingState.ready
        }).then(function(e){
            waitingState.ref.once("value").then(waitingState.update_current_data);
            var txt = game.add.text( game.width/2 , game.height/2-50-25, "Waiting for other player..." , 
                                     { font: '25px Arial', fill: '#ffffff' });
            txt.anchor.setTo(0.5, 1); 
            waitingState.txt = game.add.text( game.width/2 , game.height/2+75+25, "Press 'r' to get ready" , 
                                              { font: '25px Arial', fill: '#ffffff' });
            waitingState.txt.anchor.setTo(0.5, 1); 
            var keyr = game.input.keyboard.addKey(Phaser.Keyboard.R);
            keyr.onDown.add(function(){waitingState.change_get_ready();}, waitingState); 
        });
    },
    update_current_data: function(snapshot){
        //my_number
        var n = snapshot.numChildren();
        var number_array = [];
        var i = 0, j;
        while(i<n){
            var random = Math.floor(n*Math.random());
            if(number_array.every(function(currentValue){return (currentValue != random);})){
                number_array[i] = random;
                i++;
            }
        }
        i = 0;
        snapshot.forEach(function(childSnapshot) {
            waitingState.ref.child(childSnapshot.key+"/public").update({
                'my_number': number_array[i],
                'boom': 0,
                'battery': 0,
                'tool1': 0,
                'banana': 0,
                'key1': 0,
                'key2': 0
            });
            i++;
        });

        if( n == 1 ){
            //chest_event
            var safe_num = 40;
            var safe_type = 4;
            var safe_array_name = ["tool","battery","boom","banana"];
            var safe_array_area = [0,5,5+8,5+8+11,5+8+11+16];
    
            var danger_num = 36;
            var danger_type = 7;
            var danger_array_name = ["boom","bomb","hole","nail","freeze","key_true","key_false"];
            var danger_array_area = [0,3,3+2,3+2+12,3+2+12+8,3+2+12+8+8,3+2+12+8+8+1,3+2+12+8+8+1+2];
            
            var total_num = 81;
            var i = 0
            while( i < total_num ){
                if( i == 40 ){
                    game.global.chest_events[i] = "win";
                    i++;
                }
                else if( i == 0 || i == 8 || i == 72 || i == 80 ) {
                    game.global.chest_events[i] = "";
                    i++;
                }
                else if( i%9 == 0 || i%9 == 8 || i<9 || i>=72 || i == 10 || i == 11 || i == 19 || i == 15 || 
                    i == 16 || i == 25 || i == 55 || i == 64 || i == 65 || i == 61 || i == 69 || i == 70){//safe
                    var random = Math.floor(safe_num*Math.random()),j;
                    for(j=0;j<safe_type;j++) if( safe_array_area[j] <= random && random < safe_array_area[j+1] ) break;
                    game.global.chest_events[i] = safe_array_name[j];
                    for(;j<safe_type;j++)  safe_array_area[j+1]--;
                    safe_num--;
                    i++;
                }
                else{
                    var random = Math.floor(danger_num*Math.random()),j;
                    for(j=0;j<danger_type;j++) if( danger_array_area[j] <= random && random < danger_array_area[j+1] ) break;
                    game.global.chest_events[i] = danger_array_name[j];
                    for(;j<danger_type;j++)  danger_array_area[j+1]--;
                    danger_num--;
                    i++;
                }
            }

            //箱子
            if(1){
                var roomsArray = [1,2,3,4,5,6,7,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,41,42,43,44,45,46,47,48,49,50,51,52,53,54,55,56,57,58,59,60,61,62,63,64,65,66,67,68,69,70,71,73,74,75,76,77,78,79];
                //可以放箱子的房间号码
                for (var i=0;i<25;i++) //生成25个box 随机分布到76个房间中
                {
                    var rnd = Math.random()*75;//range 0~75
                    rnd=Math.round(rnd);
                    while(roomsArray[rnd]==-1) //index=rnd的房间 其编号==-1 说明此房间已被放置箱子，不再考虑此房间，将index+1，看下一个房间是否可放箱子
                    {
                        rnd+=1
                        if(rnd>75)rnd=0;//20180616
                    }
                    var variable=roomsArray[rnd];//生成的可放箱子的房间号
                    //20180616
                    var offsetX=(Math.random()*200-100)//range -100~100
                    var offsetY=(Math.random()*200-100)
                    var signX =0 
                    var signY =0
            
                    if(offsetX<0){signX=-1}else{signX=1};
                    if(offsetY<0){signY=-1}else{signY=1};
            
                    var boxx=offsetX+signX*64+waitingState.getChestPosition(variable).x;//range -164~-64||64~164
                    var boxy=offsetY+signY*64+waitingState.getChestPosition(variable).y;//range -164~-64||64~164
                    var pos = {x:boxx,y:boxy};
                    game.global.object_pos[i] = pos;
                    roomsArray[rnd]=-1;
                }
            }
            for(;i<85;i++) game.global.object_pos[i] = "";

            //hints
            var hints_num = 4;
            var i = 0;
            while(i<hints_num){
                var random = Math.floor(total_num*Math.random());
                if(game.global.hints.every(function(currentValue){return (currentValue != random);})){
                    if( random == 0 || random == 8 || random == 72 || random == 80 || random == 40 ) continue;
                    game.global.hints[i] = random;
                    i++;
                }
            }

            game.global.true_key = 1 + Math.floor(2*Math.random());
            waitingState.inf_ref.set({
                chest_events : game.global.chest_events,
                object_pos :　game.global.object_pos,
                true_key : game.global.true_key,
                start_time : 0,
                hints : game.global.hints
            });
        }
        waitingState.ref.on("value",waitingState.on_ref_value_change);
        waitingState.inf_ref.on("value",waitingState.on_inf_ref_value_change);
    },
    on_ref_value_change: function(snapshot){
        var i = 0,j,total_ready = 0;
        snapshot.forEach(function(childSnapshot) {
            var ready = childSnapshot.child("public").val().ready;
            var my_number = childSnapshot.child("public").val().my_number;
            var txt = childSnapshot.key;
            txt += (ready) ? " ready" : " not ready";
            total_ready += ready;

            var room_num;
            switch(my_number) {
                case 0: room_num = 0; break;
                case 1: room_num = 8; break;
                case 2: room_num = 72; break;
                case 3: room_num = 80; break;
            }
            //初始狀態
            var pos = waitingState.getChestPosition(room_num);
            var player = {
                name: childSnapshot.key,
                x: pos.x,
                y: pos.y,
                boom: 0,
                battery: 0,
                tool1: 0,
                banana: 0,
                key1: 0,
                key2: 0
            };
            
            game.global.player[my_number] = player;
            if( waitingState.name_array[i] !== 0 ) waitingState.name_array[i].destroy();
            waitingState.name_array[i] =
                game.add.text( game.width/2 , game.height/2-50+25*(i+1) , txt , { font: '25px Arial', fill: '#ffffff' });
            waitingState.name_array[i].anchor.setTo(0.5, 1); 
            if( childSnapshot.key == user_name ) game.global.my_number = my_number;
            i++;
        });
        for(j=i;j<4;j++){
            game.global.player[j] = "";
        }
        if( total_ready == i ) {
            waitingState.ref.off("value",waitingState.on_ref_value_change);
            game.global.number_of_player = total_ready;
            game.global.start_time = Date.now();
            waitingState.inf_ref.update({
                start_time : game.global.start_time
            }).then(function(){game.state.start('play');});
        }
    },
    on_inf_ref_value_change: function(snapshot){
        var data = snapshot.val();
        game.global.chest_events = data.chest_events;
        game.global.object_pos = data.object_pos;
        game.global.true_key = data.true_key;
        game.global.start_time = data.start_time;
        game.global.hints = data.hints;
    },
    //切換字
    change_get_ready: function(){
        this.ready = !this.ready;
        var new_txt = (this.ready) ? "Press 'r' to cancel" : "Press 'r' to get ready" ;
        this.txt.setText(new_txt);
        this.ref.child(user_name+"/public").update({'ready': waitingState.ready});
    },
    //得位置
    getChestPosition:function(num){
        var pos={
            x:288+(num%9)*640,
            y:416+(Math.floor(num/9))*640
        }
        return pos;
    }
};
 