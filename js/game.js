var game = null;
function changeto_gamemode(){
    if(game) { game.destroy(); game = null; }
    document.getElementById('chat').innerHTML = 
    '<div class="container text-center">'+
        '<div id="canvas">'+
            '<p class="setting-form">'+
                '<a>Which room do you want to enter?</a>'+
                '<input type="text" id="room_name" class="form-control" placeholder="" autofocus>'+
                '<button class="btn btn-lg btn-primary btn-block" onclick="gammode()">sent</button>'+
            '</p>'+
        '</div>'+
   '</div>';
}
function gammode(){
    var txtname = document.getElementById('room_name');
    if(!txtname.value) alert("Please enter room name");
    else{
        document.getElementById('chat').innerHTML =
        //0617 結合背包和canvas 
        '<div class="container "width="550px">'+
        //背包
        '<div id="package" class="row" style="background:white; height:30px">'+
            '<div class="col-2" style="background:black;color:white"><img src="assets/battery.png" ><span id="batteryNumber" style="margin:10px;text-align:center">0</span></div>'+
            '<div class="col-2" style="background:black;color:white"><img src="assets/tool1.png"><span id="tool1Number" style="margin:10px;text-align:center">0</span></div>'+
            '<div class="col-2" style="background:black;color:white"><img src="assets/boomItem.png"><span id="boomNumber" style="margin:10px;text-align:center">0</span></div>'+
            '<div class="col-2" style="background:black;color:white"><img src="assets/banana.png" ><span id="bananaNumber" style="margin:10px;text-align:center">0</span></div>'+
            '<div class="col-2" style="background:black;color:white"><img src="assets/key1.png" height="22" width="22"><span id="key1Number" style="margin:10px;text-align:center">0</span></div>'+
            '<div class="col-2" style="background:black;color:white"><img src="assets/key2.png" height="22" width="22"><span id="key2Number" style="margin:10px;text-align:center">0</span></div>'+
        '</div>'+
        //canvas部分
        '<div class="row">'+
        '<div id="canvas"></div>'+
        '</div></div>'
        // Initialize Phaser 
        game = new Phaser.Game(800, 600, Phaser.AUTO, 'canvas');
        // Define our global variable
        game.global = { room_name : "" , player : ["","","",""] , my_number : 5 , number_of_player : 4,
                        chest_events : [], object_pos : [] , true_key : 1 , start_time : 0 , hints : []}; 
        game.global.room_name = "game_rooms/";
        game.global.room_name += txtname.value;
        game.global.my_number = 5;
        game.global.chest_events = new Array(81);
        game.global.object_pos = new Array(85);
        game.global.hints = [];
        // Add all the states 
        game.state.add('boot', bootState); 
        game.state.add('load', loadState); 
        game.state.add('waiting', waitingState); 
        game.state.add('play', playState); 
        // Start the 'boot' state 
        game.state.start('boot');
    }
}